import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-personnemorale',
  templateUrl: './personnemorale.component.html',
  styleUrls: ['./personnemorale.component.css']
})
export class PersonnemoraleComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  prevform() {
    this.router.navigate(['home', 'compte']);
  }

  confirform() {
  }
}
