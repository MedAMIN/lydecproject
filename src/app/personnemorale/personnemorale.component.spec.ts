import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonnemoraleComponent } from './personnemorale.component';

describe('PersonnemoraleComponent', () => {
  let component: PersonnemoraleComponent;
  let fixture: ComponentFixture<PersonnemoraleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonnemoraleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonnemoraleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
