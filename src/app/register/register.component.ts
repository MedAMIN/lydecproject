import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  register = 'signin';
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private config: NgbCarouselConfig) {
    this.router.events.subscribe((res) => {
      if (this.router.url === '/signin') {
        this.register = 'signin';
      } else if (this.router.url === '/signup') {
        this.register = 'signup';
      }
    });
    config.interval = 3000;
    config.wrap = true;
    config.keyboard = false;
    config.pauseOnHover = false;
    config.showNavigationArrows = false;
  }

  ngOnInit() {
    this.router.navigate(['auth', 'signin']);
      }

  changeform() {
    this.router.navigate(['auth', this.register]);
  }
}
