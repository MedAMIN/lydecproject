import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  showNotif = true;
  clikedelement = 'l2';
  constructor(private router: Router) { }

  ngOnInit() {
  }

  logout() {
    this.router.navigate(['auth' , 'signin']);
  }
}
