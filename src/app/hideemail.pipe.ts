import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hideemail'
})
export class HideemailPipe implements PipeTransform {

  res: string;
  newstr: string;
  transform(value: string, ...args: any[]): any {
    if (value !== null && value !== undefined) {
      this.res = value.substr(1, value.search('@') - 2);
      this.newstr = value.replace(this.res, 'x'.repeat(this.res.length));
    }
    return this.newstr;
  }

}
