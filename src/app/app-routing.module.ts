import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RegisterComponent} from './register/register.component';
import {SigninComponent} from './signin/signin.component';
import {SignupComponent} from './signup/signup.component';
import {HomeComponent} from './home/home.component';
import {PersonnemoraleComponent} from './personnemorale/personnemorale.component';
import {CompteComponent} from './compte/compte.component';

const routes: Routes = [
  { path: '', redirectTo: 'auth/signin', pathMatch: 'full' }
  , {
  path: 'auth', component: RegisterComponent , children: [{
    path: 'signin',
    component: SigninComponent
  },
    {
      path: 'signup',
      component: SignupComponent
    }]
},
  {
    path: 'home', component: HomeComponent , children: [{
      path: 'compte', component: CompteComponent
    },
      {
      path: 'personnemorale', component: PersonnemoraleComponent,
    }]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
